# Getting Started

### Reference Documentation
This project solves the two problem in the technical evaluation form. 
I have created two REST API services. 
* All services are secured using Basic Authentication approach, you can login using 
this login credentials :
####Username: optus 
####Password: candidates

 
### Guides
To run the application please run this command:

*  `mvn spring-boot:run -f pom.xml`
* The API services are available on http://localhost:8080/counter-api/


