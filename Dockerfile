FROM openjdk:8
COPY target/word*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
