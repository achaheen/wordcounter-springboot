package com.bmp.wordcounter.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This service class used to read the content of the data file and provide three main services for the
 * 2 problems
 */
@Service
public class DataFileService {
    private final ResourceLoader resourceLoader;
    private final static Logger logger = LoggerFactory.getLogger(DataFileService.class);
    @Value("${bmp.data.file}")
    private String fileName;

    public DataFileService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public Map<String, Integer> getWordsCountInFile(LinkedList<String> searchWords) {
        logger.info("Search for {}", searchWords);
        Map<String, Integer> wordsMap = new LinkedHashMap<>();
        searchWords.forEach(word -> wordsMap.put(word, 0)); // init the map with zero value for each word
        List<String> fileWordsList = getWordList(); // get a full list of words in the file
        //creating a clone copy of words list in lowercase to find even words that are not matching the same case.
        LinkedList<String> searchWordsInLowerCase = new LinkedList<>(searchWords);
        toLowerCase(searchWordsInLowerCase);
        // looping the file words to mach our criteria
        fileWordsList.forEach(word -> {
            if (searchWordsInLowerCase.contains(word.toLowerCase().trim())) {
                //word found, getting its index
                int index = searchWordsInLowerCase.indexOf(word.toLowerCase().trim());
                String originalSearchWord = searchWords.get(index);
                logger.debug("word {} found! at index {} ", word, index);
                Integer currentCountForWord = wordsMap.get(originalSearchWord);
                wordsMap.put(originalSearchWord, ++currentCountForWord);
                logger.debug("count increased for word {} to {}", originalSearchWord, currentCountForWord);
            }
        });
        logger.debug("final count map {}", wordsMap.toString());
        fileWordsList = null;
        return wordsMap;
    }

    public Map<String, Integer> getWordCountInFile(int maxCount) {
        List<String> wordsList = getWordList();// get a list of all words
        Map<String, Integer> wordsMapCount = new HashMap<>();
        wordsList.forEach(word -> {
            String trimmedWord = word.trim().toLowerCase();
            if (wordsMapCount.containsKey(trimmedWord)) {
                Integer currentCount = wordsMapCount.get(trimmedWord);
                wordsMapCount.put(trimmedWord, ++currentCount);
            } else
                wordsMapCount.put(trimmedWord, 1);
        });

        // sorting the map in decending order (from higher to lower by value)
        final Map<String, Integer> wordsMapCountSorted = wordsMapCount.entrySet()
                .stream()
                .sorted((Map.Entry.<String, Integer>comparingByValue().reversed())).limit(maxCount)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        return wordsMapCountSorted;
    }

    /**
     * This method returns a list of all words in the file
     *
     * @return
     */
    private List<String> getWordList() {
        List<String> wordsList = new LinkedList<>();
        try {
            InputStream inputStream = getFileInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = br.readLine()) != null) {
                //   logger.debug("Line is {} ", line);
                line = line.replace(".", ""); // remove . chars
                line = line.replace(",", ""); //remove , chars
                line = line.replace(";", ""); //remove ; chars
                String[] wordsOfLine = line.split(" ");
                wordsList.addAll(Arrays.asList(wordsOfLine));
            }
            br.close();
        } catch (IOException ex) {
            logger.error("error reading file content ", ex);
        }
        logger.debug("List of word size is  {}", wordsList.size());
        return wordsList;
    }

    private void toLowerCase(List<String> words) {
        ListIterator<String> iterator = words.listIterator();
        while (iterator.hasNext()) {
            iterator.set(iterator.next().toLowerCase());
        }
    }

    private InputStream getFileInputStream() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:" + fileName);
        return resource.getInputStream();
    }
}
