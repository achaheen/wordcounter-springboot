package com.bmp.wordcounter.controller;

import com.bmp.wordcounter.model.SearchContainer;
import com.bmp.wordcounter.service.DataFileService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

@RestController
@RequestMapping("counter-api")
public class CountController {
    private static final Logger logger = LoggerFactory.getLogger(CountController.class);
    @Autowired
    private DataFileService dataFileService;

    @PostMapping("search")
    public String search(@RequestBody SearchContainer searchContainerData) throws Exception {
        Map<String, Integer> result = dataFileService.getWordsCountInFile(searchContainerData.getSearchText());
        return mapResultAsJsonObject(result).toString();
    }


    @GetMapping(value = "top/{cap}", produces = "text/csv")
    public void getTopN(@PathVariable("cap") Integer cap, HttpServletResponse response) throws IOException {

        Map<String, Integer> sortedMap = dataFileService.getWordCountInFile(cap);
        //setting the download file name.
        response.setHeader("Content-Disposition", "attachment; filename=results.csv");
        try {
            OutputStream out = response.getOutputStream();
            final StringBuilder sb = new StringBuilder();
            sortedMap.forEach((key, value) -> {

                sb.append(key);
                sb.append("|");
                sb.append(value + "\n");

            });
            out.write(sb.toString().getBytes());
            out.flush();
        } catch (Exception e) {
            logger.error("error while writing CSV response ",e);
        }

    }


    /**
     * helper method to generate the JSON reponse for the first problem,
     * the best practise to use a data binding Lib best for
     * short time I'm using a simple approach.
     * @param resultMap
     * @return
     */
    private JSONObject mapResultAsJsonObject(Map<String, Integer> resultMap) {
        JSONObject rootObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        resultMap.forEach((key, count) -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(key, count);
            jsonArray.put(jsonObject);
        });

        rootObject.put("counts", jsonArray);
        return rootObject;
    }
}
