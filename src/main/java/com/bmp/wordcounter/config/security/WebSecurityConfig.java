package com.bmp.wordcounter.config.security;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableAutoConfiguration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        /**
         * I will create an in-memory authentication repo, in real production app this must be an LDAP or Database.
         *
         */
        auth.inMemoryAuthentication()
                .passwordEncoder(new BCryptPasswordEncoder())
                .withUser("optus")
                .password("$2a$10$c5R/zORBhzmrRXlxeqMT9.vSTc3SH50IUnXmrmjzOl7G5X/nlr5Ca")
                .roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /**
         * this is the security configuration I disabled CSRF since it's a REST API and not a web application.
         */
        http.httpBasic().and().authorizeRequests()
                .antMatchers("/counter-api/**").hasRole("USER")
                .and()
                .csrf()
                .disable()
                .headers()
                .frameOptions()
                .and().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        ;
    }
}