package com.bmp.wordcounter.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

public class SearchContainer {
    @JsonProperty
    @NotNull
    private LinkedList<String> searchText;

    public SearchContainer() {
    }

    public LinkedList<String> getSearchText() {
        return searchText;
    }

    public void setSearchText(LinkedList<String> searchText) {
        this.searchText = searchText;
    }
}
