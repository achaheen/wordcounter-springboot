# Getting Started

### Reference Documentation
This project solves the two problem in the technical evaluation form. 
I have created two REST API services. 
* All services are secured using Basic Authentication approach, you can login using 
this login credentials :

* **Username: optus**
* **Password: candidates**


 
### Guides
To run the application using the **docker container** please use these commands:

Please note that the configurations uses port 80 on the host machine, 
**if port 80 is used please change the port part of the command to** `-p 8080:80`

*  `mvn clean install`
*  `docker build -t spring/wordcounts . `
* `docker run -p 80:80 -t spring/wordcounts `

To run the application in your local machine please run these commands:
**port 80 must be free, you can change this value in the application.properties file in the resources folder before build**  

*  `mvn spring-boot:run`

* The API services are available on http://localhost/counter-api/
* Maven must be installed on your local machine, otherwize run `mvn clean package` and deploy the packge to your application server.


